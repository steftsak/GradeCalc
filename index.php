<?php
session_start();
?>
<html>
<head>
	<title>Αυτόματος Υπολογισμός Βαθμού Πτυχίου</title>
	<link rel="stylesheet" type="text/css" href="index5.css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta property="og:image" content="https://www.tsaklidis.gr/grade/images/10.jpg"/>
	<meta property="og:title" content="Αυτόματος Υπολογισμός Βαθμού Πτυχίου"/>
	<meta property="og:url" content="https://www.tsaklidis.gr/grade"/>
	<meta property="og:description" content="Υπολογίστε τον βαθμού πτυχίου με ένα login, δεν χρειάζετε να έχετε περάσει τα απαιτούμενα μαθήματα, ο βαθμός υπολογίζεται σύμφωνα με τα μαθήματα που έχετε περάσει..."/>


</head>
<body>
<p class="msg0">Αυτόματος υπολογισμός βαθμού πτυχίου</p><br>

<p class="msg">
	-Ο υπολογισμός δεν έχει <b>KAMIA</b> σχέση με το ΤΕΙ ΑΜΘ ή την γραμματεία.<br><br>
	-Κανένα στοιχείο που μπορεί να αποκαλύψει την ταυτότητα κάποιου δεν αποθυκεύεται, ο υπολογισμός είναι τελείως <span style="font-weight:bold;">ΑΝΩΝΥΜΟΣ.</span><br><br>
	-Οι κωδικοί δεν αποθηκεύονται <span style="font-weight:bold;">ΠΟΥΘΕΝΑ</span> στον server, απλα προωθούνται στην γραμματεία και στην συνέχεια γίνονται οι υπολογισμοί.<br><br>
	-Αν κάποιος αμφιβάλει ας μη κάνει χρήση της σελίδας. <br><br>
	-Για τον υπολογισμό δεν χρειάζεται κάποιος να έχει περάσει όλα τα απαιτόυμενα μαθήματα.<br><br>
	-Ο τρόπος καταμέτρησης είναι αρκετά παλιός (2014) και μπορεί να μη δουλεύει πλέον σωστά με τις αλλαγές που έχουν γίνει στις σχολές. Xρειάζεται συντήρηση. <br><br>
	-Ο κώδικας <a href="https://gitlab.com/steftsak/GradeCalc">είναι διαθέσιμος</a> για αλλαγές.

</p>
<br>
<div class="wraper">
	<form action="<?php echo htmlspecialchars('check.php'); ?>" id="login" method="post" class="form">
		<table >
			<th id="th">Είσοδος Φοιτητή</th>
			<tr>
				<td>
					Όνομα Χρήστη: <input type="text" name="username" value="<?php if (isset($_SESSION['username'])) { echo $_SESSION['username']; }?>" class="input" id="username" ><br>
					Κωδικός πρόσβασης: <input type="password" name="password" value="<?php if (isset($_SESSION['password'])) { echo $_SESSION['password']; }?>"  class="input" id="password" ><br>

				</td>	
			</tr>
			<tr>
				<td>
					<span>Να με θυμάσαι: </span> <input type="checkbox" style="vertical-align: middle;"  name="rem" value="true">
				</td>
			</tr>
			<tr>

				<td >
					
					<input type="submit" value="Log in" name="login" class="button" id="login"><br>

					<?php if (isset($_GET['msg']) ) {
						if ($_GET['msg']=="empty") {
							echo '<span style="color:red">Βάλε όνομα και κωδικό</span>';
						}
						elseif ($_GET['msg']=="pass") {
							echo '<span style="color:red">Λάθος στοιχεία</span>';
						}
						else{
							echo '<span style="color:red; font-size:23;">ΓΙΑΤΙ ΜΕ ΕΝΟΧΛΕΙΣ ??</span>';
						}
						
					}?>

				</td>

			</tr>

		</table>	
	</form>
	<div style="text-align: center; color:black;">
		Για σχόλια ή παρατηρήσεις 
		<a href="https://tsaklidis.gr#contact">Επικοινωνήστε</a>
	</div>
	
</div>


</body>
</html>