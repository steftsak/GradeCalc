<?php
session_start();
?>
<html>
<head>
	<title>Αυτόματος Υπολογισμός Βαθμού Πτυχίου</title>
	<link rel="stylesheet" type="text/css" href="index4.css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta property="og:image" content="https://www.tsaklidis.gr/grade/images/10.jpg"/>
	<meta property="og:title" content="Αυτόματος Υπολογισμός Βαθμού Πτυχίου"/>
	<meta property="og:url" content="https://www.tsaklidis.gr/grade"/>
	<meta property="og:description" content="Υπολογίστε τον βαθμού πτυχίου με ένα login, δεν χρειάζετε να έχετε περάσει τα απαιτούμενα μαθήματα, ο βαθμός υπολογίζεται σύμφωνα με τα μαθήματα που έχετε περάσει..."/>


<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//tsaklidis.gr/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 3]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//tsaklidis.gr/piwik/piwik.php?idsite=3" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

</head>
<body>
<p class="msg0">Αυτόματος υπολογισμός βαθμού πτυχίου</p><br>

<p class="msg">
	-Ο υπολογισμός δεν έχει <span style="font-weight:bold;">ΚΑΜΙΑ</span> σχέση με το ΤΕΙ ΑΜΘ ή την γραμματεία.<br><br>
	-Κανένα στοιχείο που μπορεί να αποκαλύψει την ταυτότητα κάποιου δεν αποθυκεύεται ο υπολογισμός είναι τελείως <span style="font-weight:bold;">ΑΝΩΝΥΜΟΣ.</span><br><br>
	-Οι κωδικοί δεν αποθηκεύονται <span style="font-weight:bold;">ΠΟΥΘΕΝΑ</span> στον server, απλα προωθούνται στην γραμματεία και στην συνέχεια γίνονται οι υπολογισμοί.<br><br>
	-Αν κάποιος αμφιβάλει ας μη κάνει χρήση της σελίδας. <br><br>
	-Για τον υπολογισμό δεν χρειάζεται κάποιος να έχει περάσει όλα τα απαιτόυμενα μαθήματα.<br><br>
	-Η σύνδεση είναι κρυπτογραφημένη με <span style="font-weight:bold;">https</span> σε αντίθεση με την γραμματεία.<br>
</p>
<br>
<div class="wraper">
	<form action="<?php echo htmlspecialchars('debug.php'); ?>" id="login" method="post" class="form">
		<table >
			<th id="th">Είσοδος Φοιτητή</th>
			<tr>
				<td>
					Όνομα Χρήστη: <input type="text" name="username" value="<?php if (isset($_SESSION['username'])) { echo $_SESSION['username']; }?>" class="input" id="username" ><br>
					Κωδικός πρόσβασης: <input type="password" name="password" value="<?php if (isset($_SESSION['password'])) { echo $_SESSION['password']; }?>"  class="input" id="password" ><br>

				</td>	
			</tr>
			<tr>
				<td>
					<span>Να με θυμάσαι: </span> <input type="checkbox" style="vertical-align: middle;"  name="rem" value="true">
				</td>
			</tr>
			<tr>

				<td >
					
					<input type="submit" value="Log in" name="login" class="button" id="login"><br>

					<?php if (isset($_GET['msg']) ) {
						if ($_GET['msg']=="empty") {
							echo '<span style="color:red">Βάλε όνομα και κωδικό</span>';
						}
						elseif ($_GET['msg']=="pass") {
							echo '<span style="color:red">Λάθος στοιχεία</span>';
						}
						else{
							echo '<span style="color:red; font-size:23;">ΓΙΑΤΙ ΜΕ ΕΝΟΧΛΕΙΣ ??</span>';
						}
						
					}?>

				</td>

			</tr>

		</table>	
	</form>

</div>


</body>
</html>